#!/usr/bin/env python3
""" reducer.py """
import sys
#function 1
currentPasg = None  #The passenger ID currently being processed
currentCount = 0    #The current passenger's flight count
maxPasg = None      #Passenger ID with the most flights
maxCount = 0        # Highest flight count

for line in sys.stdin:
    line = line.strip()  # Remove whitespace characters from the beginning and end of a line
    passenger, count = line.split('\t', 1)  # The segmentation acts as passenger ID and count
    count = int(count)

    # If the current passenger is the same as the previous passenger, the count is accumulated
    if currentPasg == passenger:
        currentCount += count
    else:
        # When a new passenger is encountered, the maximum count is first checked and updated
        if currentPasg and currentCount > maxCount:
            maxCount = currentCount
            maxPasg = currentPasg

        # Set current passenger and count
        currentPasg = passenger
        currentCount = count

# Check the last passenger after all rows have been processed
if currentPasg and currentCount > maxCount:
    maxPasg = currentPasg
    maxCount = currentCount

# Output the passenger with the most flights and the number of flights
if maxPasg:
    print('%s\t%s' % (maxPasg, maxCount))

#Comparing the 1,2 methods, the 2 method may consume more memory when processing a large amount of data,
#because it needs to store the counts of all passengers. 
#However, for cases where the amount of data is not very huge, this method is a simple and straightforward solution
#function2
""" flightCount = {}  #Use a dictionary flightCount to store the number of flights for each passenger

for line in sys.stdin:
    line = line.strip()  # Remove whitespace characters from the beginning and end of a line
    passenger, count = line.split('\t', 1)  # The segmentation acts as passenger ID and count
    count = int(count)

    # If the passenger is already in the dictionary, add its count; otherwise, add it to the dictionary
    if passenger in flightCount:
        flightCount[passenger] += count
    else:
        flightCount[passenger] = count

# Find the passenger with the highest number of flights
maxPasg = max(flightCount, key=flightCount.get)
maxCount = flightCount[maxPasg]

# Output the passenger with the most flights and the number of flights
print('%s\t%s' % (maxPasg, maxCount)) """
