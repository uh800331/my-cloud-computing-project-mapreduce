#!/usr/bin/env python3
""" MyMapper.py """
import sys  #Import the required libraries

for line in sys.stdin:
    line = line.strip()  # Remove whitespace characters from the beginning and end of a line
    values = line.split(',')  # Separate the lines by commas
    passengerId = values[0]  # The passenger ID is the first field of each line

    print('%s\t%s' % (passengerId, 1))  # Output a count for each passenger ID  and as a parameter to redcuer