
#Import the libraries
import multiprocessing as mp
import csv

def map(row):
    try:
        # Assuming the first column is the passenger ID
        return (row[0], 1)
    except IndexError:
        # In case the row is empty or not as expected
        return None

def shuffle(mapper_out): 
    """ Organize the mapped values by key """ 
    data = {} 
    for item in filter(None, mapper_out): #Iterate over the nonempty entries in mapper_out
        key, value = item  #Unpack key-value pairs
        if key not in data: 
            data[key] = [value]  # If the key isn't already in the data dictionary, a new entry is created with the value in the list containing the current value
        else:
            data[key].append(value)  # If the key already exists, the current value is appended to the list for that key
    return data  #Returns organized data

def reduce(kv):
    key, values = kv
    #The function returns a tuple where the first element is the original key and the second element is the sum of all associated values
    return key, sum(values)

if __name__ == '__main__':
    map_in = []
    # open the CSV file
    # in my win pc file path :C:\Users\10173\Documents\cloudComputing\DataForMapReduce\data\AComp_Passenger_data_no_error.csv
    # in my mac file path :/Users/apple/Documents/cloud computing/cloudCmp/data/AComp_Passenger_data_no_error.csv
    with open(r'C:\Users\10173\Documents\cloudComputing\DataForMapReduce\data\AComp_Passenger_data_no_error.csv', 'r', encoding='utf-8') as file:
        csv_reader = csv.reader(file, delimiter=',')  #Specifies that the field delimiter in the file is a comma (,).
        map_in = list(csv_reader)  #Convert the CSV reader object to a list. Each list item is a sublist containing all the fields in a row
    
    with mp.Pool(processes=mp.cpu_count()) as pool:
        # Map step
        map_out = pool.map(map, map_in)

        # Shuffle step
        shuffle_out = shuffle(map_out)

        # Reduce step
        reduce_out = pool.map(reduce, shuffle_out.items())

    # Print the results
    for key, count in reduce_out:
        print(f"Passenger {key} travelled {count} times")
